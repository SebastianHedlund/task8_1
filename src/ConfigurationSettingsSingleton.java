public class ConfigurationSettingsSingleton {
    private String path;
    private static ConfigurationSettingsSingleton instance;

    private ConfigurationSettingsSingleton(String path){
        this.path = path;
    }

    public static ConfigurationSettingsSingleton getInstance(String path){
        if(instance == null)
            instance = new ConfigurationSettingsSingleton(path);
        return instance;
    }

    public String getPath() {
        return path;
    }
}
