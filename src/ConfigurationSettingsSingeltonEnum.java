public enum ConfigurationSettingsSingeltonEnum {
    INSTANCE;

    private String path;

    public void setPath(String path){
        this.path = path;
    }

    public String getPath(){
        return INSTANCE.path;
    }
}
