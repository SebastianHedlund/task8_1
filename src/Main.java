import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        //With simple Class
        ConfigurationSettingsSingleton setting = ConfigurationSettingsSingleton
                .getInstance("C:\\Users\\xkaid\\Documents\\Programming\\Java");

        FileProcessor fileProcessor = new FileProcessor();
        try{
            fileProcessor.printContents("testFile.txt");
        }catch(IOException e){
            System.out.println(e.getMessage());
        }

        // With Enum
        ConfigurationSettingsSingeltonEnum.INSTANCE.setPath("C:\\Users\\xkaid\\Documents\\Programming\\Java");
        FileProcessor otherFileProcessor = new FileProcessor();
        try{
            otherFileProcessor.printContents("testFile.txt");
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

    }
}
