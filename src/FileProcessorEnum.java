import java.io.*;

public class FileProcessorEnum {
    private String path = ConfigurationSettingsSingeltonEnum.INSTANCE.getPath();

    public void printContents(String fileName) throws IOException {
        File file = new File(path + "\\" + fileName);

        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr);){
            String line;
            while ((line = br.readLine()) != null){
                System.out.println(line);
            }
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

    }
}